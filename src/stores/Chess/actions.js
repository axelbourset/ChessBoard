import { SET_CHESS_QUEEN_WHITE, SET_CHESS_QUEEN_BLACK, RESET_CHESS } from '../actionTypes';

/**
 * Dispatched when the chess update
 *
 * @param  {array} queenPosition The queen data
 *
 * @return {object}  An action object with a type of SET_CHESS_QUEEN_WHITE passing the queenPosition
 */
export function updateQueenWhite(queenPosition) {
  return {
    type: SET_CHESS_QUEEN_WHITE,
    queenPosition,
  };
}

/**
 * Dispatched when the chess update
 *
 * @param  {array} queenPosition The queen data
 *
 * @return {object}  An action object with a type of SET_CHESS_QUEEN_BLACK passing the queenPosition
 */
export function updateQueenBlack(queenPosition) {
  return {
    type: SET_CHESS_QUEEN_BLACK,
    queenPosition: [...queenPosition],
  };
}

/**
 * Dispatched when we want to reset chess board
 *
 * @return {object}  An action with a type of RESET_CHESS
 */
export function resetChessBoard() {
  return {
    type: RESET_CHESS,
  };
}
