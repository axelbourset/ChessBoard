import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectChess = (state) => state.chess || initialState;

const makeSelectQueenWhite = () =>
  createSelector(selectChess, (chessState) => chessState.queenWhite);

const makeSelectQueenBlack = () => createSelector(selectChess, (chessState) => chessState);

export { selectChess, makeSelectQueenWhite, makeSelectQueenBlack };
