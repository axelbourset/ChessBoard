import produce from 'immer';
import { SET_CHESS_QUEEN_WHITE, SET_CHESS_QUEEN_BLACK, RESET_CHESS } from '../actionTypes';

// The initial state of the Chess
export const initialState = {
  queenWhite: [],
  queenBlack: [],
};

/* eslint-disable default-case, no-param-reassign */
const chessReducer = (state = initialState, action) => {
  return produce(state, (draft) => {
    switch (action.type) {
      case SET_CHESS_QUEEN_WHITE:
        draft.queenWhite = action.queenPosition;
        break;
      case SET_CHESS_QUEEN_BLACK:
        draft.queenBlack = action.queenPosition;
        break;
      case RESET_CHESS:
        return (draft = initialState);
    }
  });
};

export default chessReducer;
