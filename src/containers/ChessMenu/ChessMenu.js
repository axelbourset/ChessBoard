import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { resetChessBoard } from '../../stores/Chess/actions';
import { selectChess } from '../../stores/Chess/selectors';

import './ChessMenu.scss';

function canQueenAttackEachOther(queenBlack, queenWhite) {
  // If they are on the same X axis
  if (queenBlack[0] === queenWhite[0]) {
    return true;
  }

  // If they are on the same Y axis
  if (queenBlack[1] === queenWhite[1]) {
    return true;
  }

  // If they are on the same XY axis
  if (
    queenBlack[0] + queenBlack[1] === queenWhite[0] + queenWhite[1] ||
    Math.abs(queenBlack[0] - queenBlack[1]) === Math.abs(queenWhite[0] - queenWhite[1])
  ) {
    return true;
  }

  return false;
}

function ChessMenu({ chess, resetChessBoard }) {
  return (
    <div className="ChessMenu">
      <p className="ChessMenuTitle">Menu</p>
      <div className="ChessMenuQueenContainer">
        <div className="ChessMenuQueenItem">
          <div className="flex justifyCenter">
            <img src="https://img.icons8.com/ios/50/000000/queen.png" alt="White Queen" />
            <p className="ChessMenuQueenItemTitle">White Queen</p>
          </div>
          {chess.queenWhite.length === 0 ? (
            <p>You didn&apos;t select a position on the chessboard</p>
          ) : (
            <p>
              White queen position: {chess.queenWhite[0]}{' '}
              {String.fromCharCode(97 + chess.queenWhite[1])}
            </p>
          )}
        </div>
        <div className="ChessMenuQueenItem">
          <div className="flex justifyCenter">
            <img src="https://img.icons8.com/ios-filled/50/000000/queen.png" alt="Black Queen" />
            <p className="ChessMenuQueenItemTitle">Black Queen</p>
          </div>
          {chess.queenBlack.length === 0 ? (
            <p>You didn&apos;t select a position on the chessboard</p>
          ) : (
            <p>
              White queen position: {chess.queenBlack[0]}{' '}
              {String.fromCharCode(97 + chess.queenBlack[1])}
            </p>
          )}
        </div>
        <div className="ChessMenuQueenItem">
          <p className="ChessMenuQueenItemTitle">Result</p>
          {chess.queenBlack.length === 0 || chess.queenWhite.length === 0 ? (
            <p>Select positions of white and black queens to see if the attack is possible</p>
          ) : (
            <p>
              {canQueenAttackEachOther(chess.queenBlack, chess.queenWhite)
                ? 'Queens can attack each other'
                : "Queens can't attack each other"}
            </p>
          )}
        </div>
        <button onClick={resetChessBoard} type="button">
          <p>Reset</p>
        </button>
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  chess: selectChess,
});

export function mapDispatchToProps(dispatch) {
  return {
    resetChessBoard: () => dispatch(resetChessBoard()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

ChessMenu.propTypes = {
  chess: PropTypes.object.isRequired,
  resetChessBoard: PropTypes.func.isRequired,
};

export default compose(withConnect)(ChessMenu);
