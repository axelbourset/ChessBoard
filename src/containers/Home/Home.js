import React from 'react';
import ChessBoard from 'containers/ChessBoard/ChessBoard';
import ChessMenu from 'containers/ChessMenu/ChessMenu';

import './Home.scss';

function Home() {
  return (
    <div className="HomeSplit">
      <div className="flex8">
        <ChessBoard />
      </div>
      <div className="flex2">
        <ChessMenu />
      </div>
    </div>
  );
}

export default Home;
