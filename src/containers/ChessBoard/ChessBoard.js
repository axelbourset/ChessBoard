import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import ChessSquare from 'components/ChessSquare/ChessSquare';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { selectChess } from '../../stores/Chess/selectors';
import { updateQueenWhite, updateQueenBlack } from '../../stores/Chess/actions';

import './ChessBoard.scss';

function renderAlgebraicLine() {
  return (
    <div className="ChessBoardLine">
      <div className="flex1" />
      <div className="flex1">
        <p>a</p>
      </div>
      <div className="flex1">
        <p>b</p>
      </div>
      <div className="flex1">
        <p>c</p>
      </div>
      <div className="flex1">
        <p>d</p>
      </div>
      <div className="flex1">
        <p>e</p>
      </div>
      <div className="flex1">
        <p>f</p>
      </div>
      <div className="flex1">
        <p>g</p>
      </div>
      <div className="flex1">
        <p>h</p>
      </div>
      <div className="flex1" />
    </div>
  );
}

function renderBoard(chess, updateQueen) {
  const board = [];
  for (let i = 8; i > 0; i -= 1) {
    const boardLine = [];
    for (let j = 0; j < 8; j += 1) {
      let color = '';
      if (i % 2 === 0) {
        if (j % 2 === 0) {
          color = 'ChessColorLight';
        } else {
          color = 'ChessColorDark';
        }
      } else {
        if (j % 2 === 1) {
          color = 'ChessColorLight';
        } else {
          color = 'ChessColorDark';
        }
      }

      boardLine.push(
        <button
          className="ChessBoardButton"
          onClick={() => updateQueen([i, j])}
          disabled={chess.queenBlack[0] === i && chess.queenBlack[1] === j}
          type="button"
          key={i + '' + j}
        >
          <ChessSquare
            color={color}
            x={i}
            y={j}
            queenWhite={chess.queenWhite}
            queenBlack={chess.queenBlack}
            updateQueen={updateQueen}
          />
        </button>
      );
    }

    board.push(
      <div className="ChessBoardLine" key={i}>
        <div className="flex1">
          <p>{i}</p>
        </div>
        {boardLine}
        <div className="flex1">
          <p>{i}</p>
        </div>
      </div>
    );
  }

  return board;
}

function ChessBoard({ chess, updateQueenBlack, updateQueenWhite }) {
  const updateQueen = useCallback(
    (position) => {
      if (chess.queenBlack.length === 0) {
        updateQueenBlack(position);
      } else {
        updateQueenWhite(position);
      }
    },
    [chess, updateQueenBlack, updateQueenWhite]
  );

  return (
    <div>
      <p className="ChessBoardTitle">Chessboard</p>
      <div className="ChessBoardContainer">
        {renderAlgebraicLine()}
        {renderBoard(chess, updateQueen)}
        {renderAlgebraicLine()}
      </div>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  chess: selectChess,
});

export function mapDispatchToProps(dispatch) {
  return {
    updateQueenBlack: (newPosition) => dispatch(updateQueenBlack(newPosition)),
    updateQueenWhite: (newPosition) => dispatch(updateQueenWhite(newPosition)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

ChessBoard.propTypes = {
  chess: PropTypes.object.isRequired,
  updateQueenBlack: PropTypes.func.isRequired,
  updateQueenWhite: PropTypes.func.isRequired,
};

export default compose(withConnect)(ChessBoard);
