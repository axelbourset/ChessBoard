import React from 'react';
import PropTypes from 'prop-types';

import './ChessSquare.scss';

/**
 * Square on the Chess
 * @param {string} color Color of the Square
 * @param {number} x X position
 * @param {number} y Y position
 * @param {array} queenWhite Y position
 * @param {array} queenBlack Y position
 * @returns
 */
function ChessSquare({ color, x, y, queenWhite, queenBlack }) {
  return (
    <div className={`ChessSquare ${color}`}>
      {queenBlack[0] === x && queenBlack[1] === y && (
        <img
          src="https://img.icons8.com/ios-filled/50/000000/queen.png"
          className="ChessSquareImg"
          alt="Black Queen"
        />
      )}
      {queenWhite[0] === x && queenWhite[1] === y && (
        <img
          src="https://img.icons8.com/ios/50/000000/queen.png"
          className="ChessSquareImg"
          alt="White Queen"
        />
      )}
    </div>
  );
}

function areEqual(prevProps, nextProps) {
  // Queen black is there
  if (
    prevProps.queenBlack !== nextProps.queenBlack &&
    nextProps.queenBlack[0] === prevProps.x &&
    nextProps.queenBlack[1] === prevProps.y
  ) {
    return false;
  }
  // Queen black was there
  if (
    prevProps.queenBlack !== nextProps.queenBlack &&
    prevProps.queenBlack[0] === prevProps.x &&
    prevProps.queenBlack[1] === prevProps.y
  ) {
    return false;
  }

  // Queen white is there
  if (
    prevProps.queenWhite !== nextProps.queenWhite &&
    nextProps.queenWhite[0] === prevProps.x &&
    nextProps.queenWhite[1] === prevProps.y
  ) {
    return false;
  }
  // Queen white was there
  if (
    prevProps.queenWhite !== nextProps.queenWhite &&
    prevProps.queenWhite[0] === prevProps.x &&
    prevProps.queenWhite[1] === prevProps.y
  ) {
    return false;
  }

  return true;
}

ChessSquare.propTypes = {
  color: PropTypes.string.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  queenWhite: PropTypes.array.isRequired,
  queenBlack: PropTypes.array.isRequired,
};

export default React.memo(ChessSquare, areEqual);
