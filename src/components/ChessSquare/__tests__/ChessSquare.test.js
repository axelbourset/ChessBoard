import React from "react";
import expect from "expect";
import { shallow } from "enzyme";
import ChessSquare from "../ChessSquare";

describe("ChessSquare", () => {
  describe("Default", () => {
    let chessSquare;
    beforeAll(() => {
      chessSquare = shallow(
        <ChessSquare queenBlack={[]} queenWhite={[]} x={0} y={0} />
      );
    });
    it("renders", () => {
      expect(chessSquare.html()).toMatchSnapshot();
    });
  });
  describe("Display queen black", () => {
    let chessSquare;
    beforeAll(() => {
      chessSquare = shallow(
        <ChessSquare x={8} y={0} queenBlack={[8, 0]} queenWhite={[]} />
      );
    });
    it("renders", () => {
      expect(chessSquare.html()).toMatchSnapshot();
    });
    it("display the black Queen image", () => {
      expect(chessSquare.find("img").prop("src")).toEqual(
        "https://img.icons8.com/ios-filled/50/000000/queen.png"
      );
    });
  });
  describe("Display queen white", () => {
    let chessSquare;
    beforeAll(() => {
      chessSquare = shallow(
        <ChessSquare x={8} y={0} queenBlack={[1, 1]} queenWhite={[8, 0]} />
      );
    });
    it("renders", () => {
      expect(chessSquare.html()).toMatchSnapshot();
    });
    it("display the white Queen image", () => {
      expect(chessSquare.find("img").prop("src")).toEqual(
        "https://img.icons8.com/ios/50/000000/queen.png"
      );
    });
  });
});
